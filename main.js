

/* Циклы нужны чтобы повторять кусок кода определенное количество раз с каким то условием.
 Цикл служит для того что бы не писать один и тот же код много раз, например когда нужно вывести список чего-то*/






let userNum = +prompt("Please enter a number");

for (let i = 0; i <= userNum; i++) {
    if (i % 5 !== 0) {
        continue;
    }
    if (userNum < 5) {
        alert("Sorry, no numbers");
    }
    console.log(i);
}
